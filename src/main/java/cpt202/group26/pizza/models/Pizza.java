package cpt202.group26.pizza.models;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pizza {
    @Id//This annotation indicates the next property is id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String type;
    private int size;
    private String topping;
    private int price;
    private String description;
    //private Image image;

    public Pizza() {
        
    }

    public Pizza(int id, String name, String type, int size, String topping, int price, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.topping = topping;
        this.price = price;
        this.description = description;
    }

    public Pizza(String name, String type, int size, String topping, int price) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.topping = topping;
        this.price = price;
    }

    public BufferedImage readImage(String filename) {
        // Read an image from a file and return a Buffered image
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(filename));
        } catch (IOException e) {
            System.out.println("Read image error");
        }
        return image;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }

    public String getTopping() {
        return topping;
    }
    public void setTopping(String topping) {
        this.topping = topping;
    }

    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", name=" + name + ", type=" + type + ", size=" + size + ", topping=" + topping
                + ", price=" + price + ", description=" + description + "]";
    }

    // public Image getImage() {
    //     return image;
    // }
    // public void setImage(String filename) {
    //     this.image = readImage(filename);
    // }

    
}
