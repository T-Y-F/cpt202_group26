package cpt202.group26.pizza.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cpt202.group26.pizza.models.Pizza;
import cpt202.group26.pizza.repositories.PizzaRepo;

@Service
public class PizzaService {
    @Autowired
    private PizzaRepo pizzaRepo;

    public List<Pizza> getPizzaList() {
        return pizzaRepo.findAll();
    }

    public Pizza addNewPizza(Pizza pizza) {
        return pizzaRepo.save(pizza);
    }

    public Pizza findPizza(Integer id) {
        return pizzaRepo.findById(id).get();
    }

    public Pizza saveEdit(Pizza pizza) {
        return pizzaRepo.save(pizza);
    }
}
