package cpt202.group26.pizza.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cpt202.group26.pizza.models.User;
import cpt202.group26.pizza.repositories.UserRepo;
import cpt202.group26.pizza.security.MyUserDetails;

@Service
public class MyUserDetailsService implements UserDetailsService{
    @Autowired
    private UserRepo userRepo;

    //hook---This will be called when the user log in
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> returnedUser = userRepo.findByUserName(username);
        User user = returnedUser.orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new MyUserDetails(user);
    }
    
}
