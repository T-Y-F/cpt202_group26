package cpt202.group26.pizza.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cpt202.group26.pizza.models.Pizza;

public interface PizzaRepo extends JpaRepository<Pizza, Integer>{
    
}
