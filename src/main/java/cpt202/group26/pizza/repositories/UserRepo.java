package cpt202.group26.pizza.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cpt202.group26.pizza.models.User;

public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
}