package cpt202.group26.pizza.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    
    //localhost:8080/
    @GetMapping("")
    public String home(Model model) {
        return "userHome";
    }
}
