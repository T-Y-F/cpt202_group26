package cpt202.group26.pizza.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cpt202.group26.pizza.models.Pizza;
import cpt202.group26.pizza.services.PizzaService;

@Controller
@RequestMapping("/pizza")
public class PizzaController {
    @Autowired
    private PizzaService pizzaService;

    @GetMapping("")
    public String getPizzaPage() {
        return "pizza";
    }

    @GetMapping("/list")
    public String getList(Model model) {
        model.addAttribute("pizzaList", pizzaService.getPizzaList());
        return "allPizzas";
    }

    @GetMapping("/add")
    public String addPizza(Model model) {
        model.addAttribute("pizza", new Pizza());
        return "addPizza";
    }

    @PostMapping("/add")
    public String confirmeNewPizza(@ModelAttribute("pizza") Pizza pizza) {
        pizzaService.addNewPizza(pizza);
        return "pizza";
    }

    @GetMapping("/edit")
    public String editPizza(@RequestParam(value="id", required=false) Integer id, Model model) {
        System.out.println(pizzaService.findPizza(id));
        model.addAttribute("pizza", pizzaService.findPizza(id));
        return "editPizza";
    }

    @PostMapping("/edit")
    public String confirmEdit(@ModelAttribute("pizza") Pizza pizza) {
        pizzaService.saveEdit(pizza);
        return "pizza";
    }
}
